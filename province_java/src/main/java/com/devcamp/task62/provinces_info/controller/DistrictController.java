package com.devcamp.task62.provinces_info.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62.provinces_info.model.CDistrict;
import com.devcamp.task62.provinces_info.model.CWard;
import com.devcamp.task62.provinces_info.repository.JDistrictRepository;
import com.devcamp.task62.provinces_info.service.DistrictService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class DistrictController {
    
    @Autowired
    JDistrictRepository districtRepository;
    @Autowired
    DistrictService districtService;

    @GetMapping("/all-districts")
    public ResponseEntity <List<CDistrict>> getAllDistricts(){
        try {
            return new ResponseEntity<>(districtService.getDistrictList(), HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/ward-by-district-id")
    public ResponseEntity <Set<CWard>> getWardByDistrictId(@RequestParam(value = "id") int id){
        try {
            Set<CWard> listWards = districtService.getWardByDistrictId(id);
            if (listWards != null){
                return new ResponseEntity<>(listWards, HttpStatus.OK);
            }else{
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}


