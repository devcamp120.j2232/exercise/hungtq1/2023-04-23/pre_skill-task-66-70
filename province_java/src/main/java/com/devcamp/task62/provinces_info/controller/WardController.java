package com.devcamp.task62.provinces_info.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task62.provinces_info.model.CWard;
import com.devcamp.task62.provinces_info.repository.JWardRepository;
import com.devcamp.task62.provinces_info.service.WardService;

@RequestMapping("/")
@CrossOrigin
@RestController
public class WardController {
    
    @Autowired
    JWardRepository wardRepository;
    @Autowired
    WardService wardServicee;

    @GetMapping("/wards")
    public ResponseEntity <List<CWard>> getAllWards(){
        try {
            return new ResponseEntity<>(wardServicee.getAllWards(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

