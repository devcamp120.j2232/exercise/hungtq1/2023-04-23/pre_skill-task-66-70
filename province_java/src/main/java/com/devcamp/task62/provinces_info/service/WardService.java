package com.devcamp.task62.provinces_info.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62.provinces_info.model.CWard;
import com.devcamp.task62.provinces_info.repository.JWardRepository;

@Service
public class WardService {
    
    @Autowired
    JWardRepository wardRepository;

    public ArrayList<CWard> getAllWards(){
        ArrayList<CWard> listWards = new ArrayList<>();
        wardRepository.findAll().forEach(listWards::add);
        return listWards;
    }
}
