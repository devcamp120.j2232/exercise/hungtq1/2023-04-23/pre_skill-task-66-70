package com.devcamp.task62.provinces_info.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.task62.provinces_info.model.CDistrict;
import com.devcamp.task62.provinces_info.model.CWard;
import com.devcamp.task62.provinces_info.repository.JDistrictRepository;

@Service
public class DistrictService {
    
    @Autowired
    JDistrictRepository districtRepository;
    
    public ArrayList<CDistrict> getDistrictList(){
        ArrayList<CDistrict> listDistricts = new ArrayList<>();
        districtRepository.findAll().forEach(listDistricts::add);
        return listDistricts;
    }

    public Set<CWard> getWardByDistrictId(int id){
        CDistrict targeDistrict = districtRepository.findById(id);
        if (targeDistrict != null){
            return targeDistrict.getWard();
        }else{
            return null;
        }
    }
        

}
